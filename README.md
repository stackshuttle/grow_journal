# GrowJournal

GrowJournal is a web application that allows you to keep track
of the growth of your plants (vegetables, fruits or flowers).

To start your GrowJournal app:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Import seed data with `mix run priv/repo/seeds.exs`
  * Install Node.js dependencies with `npm install`
  * Create a super user with `mix run priv/repo/create_admin_user.exs`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Contact us

  * Join us on slack: http://ssoss-slack-invite.herokuapp.com/
  * For this repo: https://gitlab.com/stackshuttle/grow_journal/forks/new

## Deployment

On a new ubuntu server, you will need to install the following packages:

 1. Install system packages

        sudo aptitude update && sudo aptitude upgrade
        sudo aptitude install postgresql nginx make gcc git

 2. Install Erlang and elixir

        wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb && sudo dpkg -i erlang-solutions_1.0_all.deb
        # If asked for distribution name, type: `wily`
        sudo aptitude update
        sudo aptitude install esl-erlang elixir

 3. Install nvm and node

        curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh | bash
        source ~/.bashrc
        nvm install v5.9.1
        npm install

 3. Configure postgresql

        # Logs in as the postgres user
        sudo su postgres
        createuser -d -P <username> # replace <username> with your shell username
        exit
        # back as the initial user

 4. Clone repository

        git clone https://gitlab.com/stackshuttle/grow_journal.git
        cd grow_journal
        git checkout 0.1.1

 5. Install dependencies

        mix deps.get --only prod


Copy this content into `config/prod.secret.exs`.

    use Mix.Config

    # In this file, we keep production configuration that
    # you likely want to automate and keep it away from
    # your version control system.

    # You can generate a new secret by running:
    #
    #     mix phoenix.gen.secret
    config :grow_journal, GrowJournal.Endpoint,
      secret_key_base: "lHoyx8jUgRDmow9FgakKp5eCgTjmlLJmEnwpmln+4vTZfyO3yoBn1x4vdJ6Pwm6k"

Then, generate a new secret key with the command: `mix phoenix.gen.secret`.
Finally, replace the value of `secret_key_base` with the value generated.

In your `~/.bashrc` file, you will need to add two extra lines:

    export POSTGRESQL_USER=$USER
    export POSTGRESQL_PASSWORD=<yourpassword>

Then, don't forget to source your `~/.bashrc`

    source ~/.bashrc

The following commands will build your static files, compile everything, create and migrate
the database, create the digest file, import seed data and finally create an admin user.

    node_modules/brunch/bin/brunch build --production
    MIX_ENV=prod mix compile
    MIX_ENV=prod mix ecto.create
    MIX_ENV=prod mix ecto.migrate
    MIX_ENV=prod mix phoenix.digest
    MIX_ENV=prod mix run priv/repo/seeds.exs
    MIX_ENV=prod mix run priv/repo/create_admin_user.exs # Creates an admin user

Finally, you can test this configuration by running:

    PORT=4001 MIX_ENV=prod mix phoenix.server

If you can access the machine over the port 4001, you should be able to open it in your browser:
http://<ip of the machine>:4001/

The final step, is to configure nginx on port 80 to redirect internally to the port 4001.

Copy `config/growjournal.nginx.conf` to `/etc/nginx/site-available/` (as root)
The nginx config file assumes you have generated a certificate for your app (https). In this example,
we use [letsencrypt](https://letsencrypt.org/getting-started/).
Make a symlink in `/etc/nginx/site-enabled/`, and remove the default file

    ln -s /etc/nginx/sites-available/growjournal.nginx.conf /etc/nginx/sites-enabled/
    rm /etc/ngix/sites-enabled/default

Reload nginx configuration:

    service nginx reload

Now you can access your website on the port 80!

## Features

 * Keep a journal of the plants you grow
 * Get information on how to grow your plants
 * Add entries explaining what you do to your plant (and add a picture alongside with it)
 * Print QR barcodes and stick them close to your plants, so you can scan
   the barcode with your phone and add events in no time

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: http://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix

## Design

In this section, we give a bit of information on
the internal structure of GrowJournal. Let us know what you would like to see
in this section.

### Models
Models are listed in alphabetical order.

#### Disease
Fields:
 * name: string. Name of the disease.
 * plant:  reference to Plant. A disease belongs to a Plant

Diseases are associated with plants. Different plants can get different
diseases. This information is meant to be public, editable by admin users.

#### Entry
Fields:
 * short_description: string
 * when: datetime
 * long_description: string
 * picture_path: string
 * journal: reference to Journal. An event belongs to a Journal

An entry is basically anything we do a plant. This could be watering,
transplanting, adding nutrients (in hydroponics cases), feed the fishes
(in aquaponics cases)...

#### Pest
Fields:
 * name: string
 * plant: reference to Plant. A pest belongs to a plant.

Pests are associated with plants. Different plants can get different pests.
This information is meant to be public, editable by admin users.

#### Plant
Fields:
 * name: string
 * picture: string
 * description: string
 * diseases: reference back to Disease. A plant has many (common) diseases
 * pests: reference back to Pest. A plant has many (common) pests
 * varieties: reference back to Variety. A plant has many varieties
 * journals: reference back to Journals. A plant has many journals

#### User
Fields:
 * username: string
 * email: string
 * password: virtual field
 * password_hash: string
 * journal: reference back to Journal. A user has many journals
   (plants they grow).
 * is_admin: boolean

Users are GrowJournal users, they can log in, logout,
add plants to their account (i.e they're growing them).
The password is a virtual field is not actually saved to the database.
if `is_admin` is set to true, the user has access to `/admin`

#### Journal
Fields:
 * user: reference to User. A Journal belongs to a User.
 * plant: reference to Plant. A Journal belongs to a Plant.
 * entries: reference back to Entry. A Journal has many entries.
 * qrcode_path: string

A Journal keeps track of a growth of a plant.
Every journal has a qr bar code that redirects the user to the page
of the journal, allowing the user to add events
to his journals with a QR bar code application on their phone.

#### Variety
Fields:
 * name: string
 * plant: reference to Plant. A variety belongs to a plant.

Plants have different varieties.


### Controllers

#### admin

`admin` is a folder containing controllers only accessible to admin users
It is represented as a scope in the router.
It defines all actions that are available to administrators and is available
in the `/admin/` scope.

In this folder, we have a few controllers:

 * `disease_controller.ex`: `new`/`create`/`edit`/`update`/`delete` actions.
   `index` and `show` should be removed.
 * `pest_controller.ex`: `new`/`create`/`edit`/`update`/`delete` actions.
   `index` and `show` should be removed.
 * `plant_controller.ex`: all actions on plants. In `show`, we are also listing
   the plant's diseases/pests and varieties.
 * `user_controller.ex`: all actions on users.
 * `variety_controller.ex`: `new`/`edit`/`delete` actions.
   `index` and `show` should be removed.

#### user


`user` is a folder containing controllers only accessible to them
It defines the `change_password` and `update_change_password` actions.

`auth_controller.ex`: inspired by “Programming Phoenix” by Chris McCord, Bruce Tate and José Valim.

`media_files_controller.ex`: a controller for media files. Media files are files available to users,
such as uploaded images, qr barcodes and so on.
This should ensure a user is only downloads their own files.

`page_controller.ex`: front page controller. Created by phoenix.
`plant_controller.ex`: the plant controller is for the
public information related to plants.
Note: we don't need disease/pest/variety controllers because this is part of

`session_controller.ex`: inspired by “Programming Phoenix” by Chris McCord, Bruce Tate and José Valim.



## Things to do:

 * Improve style (switch to bourbon in the mean time)
 * Convert website to react
 * make mobile app?
 * Ideas? Contribute to this document!
