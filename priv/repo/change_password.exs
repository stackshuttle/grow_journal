alias GrowJournal.User
alias GrowJournal.Repo
import Ecto.Query, only: [from: 2]

username = IO.gets "What user do you want to change the password?\n"
password = IO.gets "Choose a new password:\n"

username = String.replace(username, "\n", "")
password = String.replace(password, "\n", "")

query = from user in User,
  where: user.username == ^username
users = Repo.all(query)
user = nil
cond do
  users != [] -> user = hd users
  true -> raise "No user found"
end


changeset = User.password_changeset(user, %{password: password})

cond do
  changeset.errors != [] -> IO.inspect changeset.errors
  true -> Repo.update(changeset)
end
