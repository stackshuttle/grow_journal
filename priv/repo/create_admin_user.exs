alias GrowJournal.User
alias GrowJournal.Repo

username = IO.gets "Choose a username for your new admin user:\n"
password = IO.gets "Choose a password:\n"

username = String.replace(username, "\n", "")
password = String.replace(password, "\n", "")

changeset = User.admin_registration_changeset(%User{}, %{username: username, password: password})
Repo.insert!(changeset)
