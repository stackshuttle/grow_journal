defmodule GrowJournal.Repo.Migrations.CreateEntries do
  use Ecto.Migration

  def change do
    create table(:entries) do
      add :short_description, :string
      add :when, :datetime
      add :long_description, :text
      add :picture_path, :string

      timestamps
    end
  end
end
