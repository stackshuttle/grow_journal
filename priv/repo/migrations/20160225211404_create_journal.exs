defmodule GrowJournal.Repo.Migrations.CreateJournal do
  use Ecto.Migration

  def change do
    create table(:journals) do
      add :user_id, references(:users, on_delete: :delete_all)
      add :plant_id, references(:plants, on_delete: :nothing)

      timestamps
    end
    create index(:journals, [:user_id])
    create index(:journals, [:plant_id])

  end
end
