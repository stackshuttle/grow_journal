defmodule GrowJournal.Repo.Migrations.AddUserPlantFieldToEntries do
  use Ecto.Migration

  def change do
    alter table(:entries) do
      add :journal_id, references(:journals, on_delete: :delete_all)
    end
    create index(:entries, [:journal_id])
  end
end
