defmodule GrowJournal.Repo.Migrations.AddJournalName do
  use Ecto.Migration

  def change do
    alter table(:journals) do
      add :name, :string
    end
  end
end
