# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     GrowJournal.Repo.insert!(%GrowJournal.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias GrowJournal.Repo
alias GrowJournal.Plant

media_folder = System.cwd() <> "/priv/repo/media/plants/"

lettuce_path = "/plants/Lettuce.jpg"
Repo.get_by(Plant, name: "Lettuce") || Repo.insert!(
  %GrowJournal.Plant{
    name: "Lettuce",
    description: """
    Lettuce is a vegetable that is very healthy to eat.
    Both the stems and the leaves can be eaten.
    """,
    picture: lettuce_path
  }
)
:ok = File.cp media_folder <> "Lettuce.jpg", System.cwd() <> "/media" <> lettuce_path

strawberry_path = "/plants/Strawberry.jpg"
Repo.get_by(Plant, name: "Strawberry") || Repo.insert!(
  %GrowJournal.Plant{
    name: "Strawberry",
    description: """
    A strawberry is a plant that grows fruit that people eat.
    The fruit of a strawberry is red when ripe, and has edible seeds on the outside.
    """,
    picture: strawberry_path
  }
)
:ok = File.cp media_folder <> "Strawberry.jpg", System.cwd() <> "/media" <> strawberry_path
