defmodule GrowJournal.API.DiseaseControllerTest do
  use GrowJournal.ConnCase

  @plant %{name: "Test plant", description: "Test description"}
  @diseases [%{name: "Disease 1"}, %{name: "Disease 2"}]

  setup %{conn: conn} do
    plant = insert_plant(@plant)

    Enum.each(@diseases, fn disease_params -> create_plant_and_diseases(plant, disease_params) end)
    {:ok, conn: conn}
  end

  defp create_plant_and_diseases(plant, disease_params) do
    plant
    |> build_assoc(:diseases, disease_params)
    |> Repo.insert!()
  end

  defp get_plant_id(conn, path) do
    conn = get conn, path
    {:ok, json} = conn.resp_body |> Poison.decode()
    [plant] = json["data"]
    plant["id"]
  end

  test "lists all disease on api index", %{conn: conn} do
    plant_id = get_plant_id(conn, api_plant_path(conn, :index))

    conn = get conn, api_plant_disease_path(conn, :index, plant_id)
    {:ok, json} = conn.resp_body |> Poison.decode()
    [disease, other_disease] = json["data"]
    assert disease["attributes"]["name"] == Enum.at(@diseases, 0)[:name]
    assert other_disease["attributes"]["name"] == Enum.at(@diseases, 1)[:name]
  end

  test "Shows details of first plant found on index", %{conn: conn} do
    plant_id = get_plant_id(conn, api_plant_path(conn, :index))

    conn = get conn, api_plant_disease_path(conn, :index, plant_id)
    {:ok, json} = conn.resp_body |> Poison.decode()
    [disease, _] = json["data"]

    conn = get conn, api_plant_disease_path(conn, :show, plant_id, disease["id"])
    {:ok, json} = conn.resp_body |> Poison.decode()
    disease = json["data"]
    assert disease["attributes"]["name"] == Enum.at(@diseases, 0)[:name]
  end
end
