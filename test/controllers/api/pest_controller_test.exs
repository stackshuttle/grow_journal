defmodule GrowJournal.API.PestControllerTest do
  use GrowJournal.ConnCase

  @plant %{name: "Test plant", description: "Test description"}
  @pests [%{name: "Pest 1"}, %{name: "Pest 2"}]

  setup %{conn: conn} do
    plant = insert_plant(@plant)

    Enum.each(@pests, fn pest_params -> create_plant_and_pests(plant, pest_params) end)
    {:ok, conn: conn}
  end

  defp create_plant_and_pests(plant, pest_params) do
    plant
    |> build_assoc(:pests, pest_params)
    |> Repo.insert!()
  end

  defp get_plant_id(conn, path) do
    conn = get conn, path
    {:ok, json} = conn.resp_body |> Poison.decode()
    [plant] = json["data"]
    plant["id"]
  end

  test "lists all disease on api index", %{conn: conn} do
    plant_id = get_plant_id(conn, api_plant_path(conn, :index))

    conn = get conn, api_plant_pest_path(conn, :index, plant_id)
    {:ok, json} = conn.resp_body |> Poison.decode()
    [pest, other_pest] = json["data"]
    assert pest["attributes"]["name"] == Enum.at(@pests, 0)[:name]
    assert other_pest["attributes"]["name"] == Enum.at(@pests, 1)[:name]
  end

  test "Shows details of first plant found on index", %{conn: conn} do
    plant_id = get_plant_id(conn, api_plant_path(conn, :index))

    conn = get conn, api_plant_pest_path(conn, :index, plant_id)
    {:ok, json} = conn.resp_body |> Poison.decode()
    [pest, _] = json["data"]

    conn = get conn, api_plant_pest_path(conn, :show, plant_id, pest["id"])
    {:ok, json} = conn.resp_body |> Poison.decode()
    pest = json["data"]
    assert pest["attributes"]["name"] == Enum.at(@pests, 0)[:name]
  end
end
