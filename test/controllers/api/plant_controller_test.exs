defmodule GrowJournal.PlantControllerTest do
  use GrowJournal.ConnCase

  @plants [
    %{name: "Test plant", description: "Test description"},
    %{name: "Test plant 2", description: "Test description 2"}
  ]

  setup %{conn: conn} do
    Enum.each(@plants, fn plant_params -> create_plant_and_related(plant_params) end)
    {:ok, conn: conn}
  end

  defp create_plant_and_related(plant_params) do
    plant = insert_plant(plant_params)
    plant
    |> build_assoc(:diseases, name: "Disease")
    |> Repo.insert!()
    plant
    |> build_assoc(:pests, name: "Pest")
    |> Repo.insert!()
    plant
    |> build_assoc(:varieties, name: "Variety")
    |> Repo.insert!()
  end

  test "lists all plants on api index", %{conn: conn} do
    conn = get conn, api_plant_path(conn, :index)
    {:ok, json} = conn.resp_body |> Poison.decode()
    [plant, other_plant] = json["data"]
    assert plant["attributes"]["name"] == Enum.at(@plants, 0)[:name]
    assert plant["attributes"]["description"] == Enum.at(@plants, 0)[:description]
    assert Enum.count(plant["relationships"]["diseases"]) == 1
    assert Enum.count(plant["relationships"]["pests"]) == 1
    assert Enum.count(plant["relationships"]["varieties"]) == 1
    assert other_plant["attributes"]["name"] == Enum.at(@plants, 1)[:name]
    assert other_plant["attributes"]["description"] == Enum.at(@plants, 1)[:description]
    assert Enum.count(other_plant["relationships"]["diseases"]) == 1
    assert Enum.count(other_plant["relationships"]["pests"]) == 1
    assert Enum.count(other_plant["relationships"]["varieties"]) == 1
  end

  test "Shows details of first plant found on index", %{conn: conn} do
    conn_index = get conn, api_plant_path(conn, :index)
    {:ok, index_json} = conn_index.resp_body |> Poison.decode()
    [plant, _] = index_json["data"]
    plant_id = plant["id"]

    conn = get conn, api_plant_path(conn, :show, plant_id)
    {:ok, json} = conn.resp_body |> Poison.decode()
    plant = json["data"]
    assert plant["attributes"]["name"] == Enum.at(@plants, 0)[:name]
    assert plant["attributes"]["description"] == Enum.at(@plants, 0)[:description]
    assert Enum.count(plant["relationships"]["diseases"]) == 1
    assert Enum.count(plant["relationships"]["pests"]) == 1
    assert Enum.count(plant["relationships"]["varieties"]) == 1
  end
end
