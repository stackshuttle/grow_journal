defmodule GrowJournal.VarietyControllerTest do
  use GrowJournal.ConnCase

  @plant %{name: "Test plant", description: "Test description"}
  @varieties [%{name: "Variety 1"}, %{name: "Variety 2"}]

  setup %{conn: conn} do
    plant = insert_plant(@plant)

    Enum.each(@varieties, fn variety_params -> create_plant_and_varieties(plant, variety_params) end)
    {:ok, conn: conn}
  end

  defp create_plant_and_varieties(plant, variety_params) do
    plant
    |> build_assoc(:varieties, variety_params)
    |> Repo.insert!()
  end

  defp get_plant_id(conn, path) do
    conn = get conn, path
    {:ok, json} = conn.resp_body |> Poison.decode()
    [plant] = json["data"]
    plant["id"]
  end

  test "lists all disease on api index", %{conn: conn} do
    plant_id = get_plant_id(conn, api_plant_path(conn, :index))

    conn = get conn, api_plant_variety_path(conn, :index, plant_id)
    {:ok, json} = conn.resp_body |> Poison.decode()
    [variety, other_variety] = json["data"]
    assert variety["attributes"]["name"] == Enum.at(@varieties, 0)[:name]
    assert other_variety["attributes"]["name"] == Enum.at(@varieties, 1)[:name]
  end

  test "Shows details of first plant found on index", %{conn: conn} do
    plant_id = get_plant_id(conn, api_plant_path(conn, :index))

    conn = get conn, api_plant_variety_path(conn, :index, plant_id)
    {:ok, json} = conn.resp_body |> Poison.decode()
    [variety, _] = json["data"]

    conn = get conn, api_plant_variety_path(conn, :show, plant_id, variety["id"])
    {:ok, json} = conn.resp_body |> Poison.decode()
    variety = json["data"]
    assert variety["attributes"]["name"] == Enum.at(@varieties, 0)[:name]
  end
end
