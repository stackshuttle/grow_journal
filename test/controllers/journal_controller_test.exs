defmodule GrowJournal.JournalControllerTest do
  use GrowJournal.ConnCase

  setup %{ conn: conn} = config do
    if config[:login_as] do
      user = insert_user(username: "max")
      conn = assign(conn(), :current_user, user)
      {:ok, conn: conn, user: user}
    else
      :ok
    end
  end

  @tag login_as: "max"
  test "lists all user's journals on index", %{conn: conn, user: user} do
    plant = insert_plant(%{name: "Test plant", description: "Test description"})
    other_plant = insert_plant(%{name: "Test plant 2", description: "Test description 2"})
    user_journal  = insert_journal(user, %{plant_id: plant.id, name: "Journal Test plant"})
    |> Repo.preload(:plant)
    other_journal = insert_journal(insert_user(username: "other"), %{plant_id: other_plant.id, name: "Other"})
    |> Repo.preload(:plant)

    conn = get conn, journal_path(conn, :index)
    assert html_response(conn, 200) =~ ~r/My Journals/
    assert String.contains?(conn.resp_body, user_journal.name)
    refute String.contains?(conn.resp_body, other_journal.name)
  end


  test "requires user authentication on all actions", %{conn: conn} do
    Enum.each([
      get(conn, journal_path(conn, :new)),
      get(conn, journal_path(conn, :edit, "123")),
      get(conn, journal_path(conn, :show, "123")),
      get(conn, journal_path(conn, :edit, "123")),
      put(conn, journal_path(conn, :update, "123", %{})),
      post(conn, journal_path(conn, :create, %{})),
      delete(conn, journal_path(conn, :delete, "123")),
    ], fn conn ->
      assert html_response(conn, 302)
      assert conn.halted
    end)
  end
end
