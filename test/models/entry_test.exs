defmodule GrowJournal.EntryTest do
  use GrowJournal.ModelCase

  alias GrowJournal.Entry

  @valid_attrs %{
    short_description: "some content",
    long_description: "some content",
    when: "2010-04-17 14:00:00",
    journal_id: 1,
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Entry.changeset(%Entry{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Entry.changeset(%Entry{}, @invalid_attrs)
    refute changeset.valid?
  end
end
