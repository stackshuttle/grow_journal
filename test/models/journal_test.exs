defmodule GrowJournal.JournalTest do
  use GrowJournal.ModelCase

  alias GrowJournal.Journal

  @valid_attrs %{
    plant_id: 1,
    journal_id: 1,
  }
  @invalid_attrs %{plant_id: "string"}

  test "changeset with valid attributes" do
    changeset = Journal.changeset(%Journal{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Journal.changeset(%Journal{}, @invalid_attrs)
    refute changeset.valid?
  end
end
