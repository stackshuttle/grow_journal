defmodule GrowJournal.TestHelpers do
  alias GrowJournal.Repo

  def insert_plant(attrs \\ %{}) do
    %GrowJournal.Plant{}
    |> GrowJournal.Plant.changeset(attrs)
    |> Repo.insert!()
  end

  def insert_user(attrs \\ %{}) do
    changes = Dict.merge(%{
      username: "user#{Base.encode16(:crypto.rand_bytes(8))}",
      password: "supersecret",
    }, attrs)

    %GrowJournal.User{}
    |> GrowJournal.User.registration_changeset(changes)
    |> Repo.insert!()
  end

  def insert_journal(user, attrs \\ %{}) do
    user
    |> Ecto.build_assoc(:journals, attrs)
    |> Repo.insert!()
  end
end
