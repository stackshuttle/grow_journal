defmodule GrowJournal.Admin.DiseaseController do
  use GrowJournal.Web, :controller

  alias GrowJournal.Disease
  alias GrowJournal.Plant

  import Ecto.Model, only: [build: 2]

  plug :scrub_params, "plant_id"
  plug :scrub_params, "disease" when action in [:create, :update]

  def new(conn, %{"plant_id" => plant_id}) do
    changeset = Disease.changeset(%Disease{})
    plant = Repo.get!(Plant, plant_id)
    render(conn, "new.html", changeset: changeset, plant: plant)
  end

  def create(conn, %{"disease" => disease_params, "plant_id" => plant_id}) do
    plant = Repo.get!(Plant, plant_id)
    changeset = build(plant, :diseases)
    |> Disease.changeset(disease_params)

    case Repo.insert(changeset) do
      {:ok, _disease} ->
        conn
        |> put_flash(:info, "Disease created successfully.")
        |> redirect(to: admin_plant_path(conn, :show, plant_id))
      {:error, changeset} ->
        plant = Repo.get!(Plant, plant_id)
        render(conn, "new.html", changeset: changeset, plant: plant)
    end
  end

  def edit(conn, %{"id" => id, "plant_id" => plant_id}) do
    disease = Repo.get!(Disease, id)
    changeset = Disease.changeset(disease)
    plant = Repo.get!(Plant, plant_id)
    render(conn, "edit.html", disease: disease, changeset: changeset, plant: plant)
  end

  def update(conn, %{"id" => id, "disease" => disease_params,
                     "plant_id" => plant_id}) do
    disease = Repo.get!(Disease, id)
    changeset = Disease.changeset(disease, disease_params)
    plant = Repo.get!(Plant, plant_id)

    case Repo.update(changeset) do
      {:ok, _disease} ->
        conn
        |> put_flash(:info, "Disease updated successfully.")
        |> redirect(to: admin_plant_path(conn, :show, plant_id))
      {:error, changeset} ->
        render(conn, "edit.html", disease: disease, changeset: changeset, plant: plant)
    end
  end

  def delete(conn, %{"id" => id, "plant_id" => plant_id}) do
    disease = Repo.get!(Disease, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(disease)

    conn
    |> put_flash(:info, "Disease deleted successfully.")
    |> redirect(to: admin_plant_disease_path(conn, :index, plant_id))
  end
end
