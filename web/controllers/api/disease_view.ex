defmodule GrowJournal.API.DiseaseController do
  use GrowJournal.Web, :controller

  alias GrowJournal.Disease

  def index(conn, _params) do
    diseases = Repo.all(Disease)
    render(conn, data: diseases)
  end

  def show(conn, %{"id" => id}) do
    disease = Repo.get!(Disease, id)
    render(conn, data: disease)
  end
end
