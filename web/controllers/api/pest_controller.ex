defmodule GrowJournal.API.PestController do
  use GrowJournal.Web, :controller

  alias GrowJournal.Pest

  def index(conn, _params) do
    pests = Repo.all(Pest)
    render(conn, data: pests)
  end

  def show(conn, %{"id" => id}) do
    pest = Repo.get!(Pest, id)
    render(conn, data: pest)
  end
end
