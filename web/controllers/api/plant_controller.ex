defmodule GrowJournal.API.PlantController do
  use GrowJournal.Web, :controller

  alias GrowJournal.Plant

  def index(conn, _params) do
    plants = Repo.all(Plant)
    |> Repo.preload(:diseases)
    |> Repo.preload(:pests)
    |> Repo.preload(:varieties)
    render(conn, data: plants)
  end

  def show(conn, %{"id" => id}) do
    plant = Repo.get!(Plant, id)
    plant = Repo.preload plant, :diseases
    plant = Repo.preload plant, :pests
    plant = Repo.preload plant, :varieties
    render(conn, data: plant)
  end
end
