defmodule GrowJournal.API.DiseaseSerializer do
  use JaSerializer

  has_one :plant, field: :plant_id, type: "plants"
  attributes [:name]
end