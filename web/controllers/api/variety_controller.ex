defmodule GrowJournal.API.VarietyController do
  use GrowJournal.Web, :controller

  alias GrowJournal.Variety

  def index(conn, _params) do
    varieties = Repo.all(Variety)
    render(conn, data: varieties)
  end

  def show(conn, %{"id" => id}) do
    variety = Repo.get!(Variety, id)
    render(conn, data: variety)
  end
end
