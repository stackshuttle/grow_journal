defmodule GrowJournal.EntryController do
  use GrowJournal.Web, :controller

  alias GrowJournal.Entry

  import GrowJournal.JournalController, only: [user_journals: 1]

  plug :scrub_params, "entry" when action in [:create, :update]

  def action(conn, _) do
    apply(__MODULE__, action_name(conn), [conn, conn.params, conn.assigns.current_user])
  end

  def new(conn, %{"journal_id" => journal_id}, user) do
    changeset = Entry.changeset(%Entry{})
    journal = Repo.get!(user_journals(user), journal_id)
    render(conn, "new.html", changeset: changeset, journal: journal)
  end

  def create(conn, %{"entry" => entry_params, "journal_id" => journal_id}, user) do
    file = entry_params["picture_path"]
    full_path = ""
    if file != nil do
      short_path = "#{conn.assigns.current_user.id}/journals/#{journal_id}/pictures/#{file.filename}"
      full_path = System.cwd() <> "/media/" <> short_path
      entry_params = %{entry_params| "picture_path" => short_path}
    end

    journal = Repo.get!(user_journals(user), journal_id)
    changeset = build_assoc(journal, :entries)
                |> Entry.changeset(entry_params)
    case Repo.insert(changeset) do
      {:ok, _entry} ->
        if file != nil do
          :ok = File.cp file.path, full_path
        end
        conn
        |> put_flash(:info, "Entry created successfully.")
        |> redirect(to: journal_path(conn, :show, journal))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset, journal: journal)
    end
  end

  def edit(conn, %{"id" => id, "journal_id" => journal_id}, user) do
    journal = Repo.get!(user_journals(user), journal_id)
    entry = get_journal_related_entries(journal, id)
    changeset = Entry.changeset(entry)
    render(conn, "edit.html", entry: entry, changeset: changeset,
           journal: journal)
  end

  def update(conn, %{"id" => id, "entry" => entry_params,
                     "journal_id" => journal_id}, user) do

    journal = Repo.get!(user_journals(user), journal_id)
    entry = get_journal_related_entries(journal, id)
    changeset = Entry.changeset(entry, entry_params)

    case Repo.update(changeset) do
      {:ok, _entry} ->
        conn
        |> put_flash(:info, "Entry updated successfully.")
        |> redirect(to: journal_path(conn, :show, journal_id))
      {:error, changeset} ->
        render(conn, "edit.html", entry: entry, changeset: changeset,
               journal: journal)
    end
  end

  def delete(conn, %{"id" => id, "journal_id" => journal_id}, user) do
    journal = Repo.get!(user_journals(user), journal_id)
    entry = get_journal_related_entries(journal, id)
    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(entry)

    conn
    |> put_flash(:info, "Entry deleted successfully.")
    |> redirect(to: journal_path(conn, :index))
  end

  defp get_journal_related_entries(journal, id) do
    entry_query = from e in Entry, where: e.id == ^id
    journal_with_entries = Repo.preload(journal, entries: entry_query)
    hd journal_with_entries.entries
  end
end
