defmodule GrowJournal.JournalController do
  use GrowJournal.Web, :controller

  alias GrowJournal.Journal
  alias GrowJournal.Plant
  alias GrowJournal.Entry

  import Ecto.Model, only: [build: 2]

  plug :scrub_params, "journal" when action in [:create, :update]

  def action(conn, _) do
    apply(__MODULE__, action_name(conn), [conn, conn.params, conn.assigns.current_user])
  end

  defp build_plant_absolute_url(conn, journal_id) do
    port = ""
    if conn.port != 80 do
      port = ":#{conn.port}"
    end

    http = "http://"
    if conn.scheme != :http do
      http = "https://"
    end

    "#{http}#{conn.host}#{port}#{journal_path(conn, :show, journal_id)}"
  end

  def index(conn, _params, user) do
    entry_query = from entry in Entry, order_by: [desc: entry.when]

    journals = Repo.all from journal in user_journals(user),
               preload: [:plant, entries: ^entry_query]

    render(conn, "index.html", journals: journals)
  end

  def new(conn, _params, _user) do
    changeset = Journal.changeset(%Journal{})
    plants = Repo.all(Plant)
    render(conn, "new.html", changeset: changeset, plants: plants)
  end

  defp build_media_folder_structure_for_user(user, journal) do
    # folder to create if it doesn't exist
    folder_path = "#{System.cwd}/media/#{user.id}/journals/#{journal.id}/pictures"
    if not File.exists?(folder_path) do
      :ok = File.mkdir_p(folder_path)
    end
  end

  def create(conn, %{"journal" => journal_params}, user) do
    changeset = build(user, :journals)
    |> Journal.changeset(journal_params)

    case Repo.insert(changeset) do
      {:ok, journal} ->
        build_media_folder_structure_for_user(conn.assigns.current_user,
                                              journal)
        url = build_plant_absolute_url(conn, journal.id)
        qrcode = Journal.create_qrcode(conn.assigns.current_user.id,
                                       journal.id, url)
        journal = %Journal{journal| :qrcode_path => qrcode}
        Repo.update(journal)
        conn
        |> put_flash(:info, "Journal created successfully.")
        |> redirect(to: journal_path(conn, :index))
      {:error, changeset} ->
        plants = Repo.all(Plant)
        render(conn, "new.html", changeset: changeset, plants: plants)
    end
  end

  def show(conn, %{"id" => id}, user) do
    journal = Repo.get!(user_journals(user), id)
    |> Repo.preload(:plant)
    |> Repo.preload(:entries)
    render(conn, "show.html", journal: journal)
  end

  def edit(conn, %{"id" => id}, user) do
    journal = Repo.get!(user_journals(user), id)
    plants = Repo.all(Plant)
    changeset = Journal.changeset(journal)
    render(conn, "edit.html", journal: journal, changeset: changeset,
           plants: plants)
  end

  def update(conn, %{"id" => id, "journal" => journal_params}, user) do
    journal = Repo.get!(user_journals(user), id)
    changeset = Journal.changeset(journal, journal_params)

    case Repo.update(changeset) do
      {:ok, journal} ->
        conn
        |> put_flash(:info, "Journal updated successfully.")
        |> redirect(to: journal_path(conn, :show, journal))
      {:error, changeset} ->
        render(conn, "edit.html", journal: journal, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}, user) do
    journal = Repo.get!(user_journals(user), id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(journal)

    conn
    |> put_flash(:info, "Journal deleted successfully.")
    |> redirect(to: journal_path(conn, :index))
  end

  def qrcodes(conn, _params, user) do
    journals = Repo.all(user_journals(user))
    |> Repo.preload(:plant)
    render(conn, "qrcodes.html", journals: journals)
  end

  def user_journals(user) do
    assoc(user, :journals)
  end
end
