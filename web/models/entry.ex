defmodule GrowJournal.Entry do
  use GrowJournal.Web, :model

  schema "entries" do
    field :short_description, :string
    field :when, Ecto.DateTime
    field :long_description, :string
    field :picture_path, :string
    belongs_to :journal, GrowJournal.Journal

    timestamps
  end

  @required_fields ~w(short_description when)
  @optional_fields ~w(long_description picture_path)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
