defmodule GrowJournal.Journal do
  use GrowJournal.Web, :model

  schema "journals" do
    belongs_to :user, GrowJournal.User
    belongs_to :plant, GrowJournal.Plant
    has_many :entries, GrowJournal.Entry
    field :qrcode_path, :string
    field :name, :string

    timestamps
  end

  @required_fields ~w()
  @optional_fields ~w(qrcode_path plant_id name)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end

  def create_qrcode(user_id, journal_id, url) do
    qrcode = :qrcode.encode(url)
    png = :qrcode_demo.simple_png_encode(qrcode)
    short_path = "/#{user_id}/journals/#{journal_id}/qrcode.png"

    # folder to create if it doesn't exist
    folder_path = "#{System.cwd}/media/#{user_id}/journals/#{journal_id}/"
    full_path = "#{folder_path}/qrcode.png"
    :ok = :file.write_file(full_path, png)
    short_path
  end
end
