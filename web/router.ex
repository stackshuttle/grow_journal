defmodule GrowJournal.Router do
  use GrowJournal.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug GrowJournal.Auth, repo: GrowJournal.Repo
  end

  pipeline :api do
    plug :accepts, ["json-api"]
    plug JaSerializer.ContentTypeNegotiation
    plug JaSerializer.Deserializer
  end

  scope "/", GrowJournal do
    pipe_through :browser

    get "/", PageController, :index
    get "/media/*filepath", MediaFilesController, :download

    resources "/users", UserController, only: [:new, :create]
    resources "/session", SessionController, except: [:update, :edit]

    resources "/plants", PlantController, only: [:index, :show]
  end

  scope "/admin", GrowJournal.Admin, as: :admin do
    pipe_through [:browser, :authenticate_user, :admin_user]

    resources "/users", UserController
    resources "/plants", PlantController do
      resources "/diseases", DiseaseController, except: [:index, :show]
      resources "/pests", PestController, except: [:index, :show]
      resources "/varieties", VarietyController, except: [:index, :show]
    end
  end

  scope "/", GrowJournal do
    pipe_through [:browser, :authenticate_user]

    get "/change_password", UserHomeController, :change_password
    post "/change_password", UserHomeController, :update_change_password
    put "/change_password", UserHomeController, :update_change_password

    get "/qrcodes", JournalController, :qrcodes
    resources "/journals", JournalController do
      resources "/entries", EntryController, except: [:index]
    end
  end

  # Other scopes may use custom stacks.
  scope "/api", GrowJournal, as: :api do
    pipe_through :api

    resources "/plants", API.PlantController, only: [:index, :show] do
      resources "/diseases", API.DiseaseController, only: [:index, :show]
      resources "/pests", API.PestController, only: [:index, :show]
      resources "/varieties", API.VarietyController, only: [:index, :show]
    end
  end
end
