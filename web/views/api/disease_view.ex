defmodule GrowJournal.API.DiseaseView do
  use GrowJournal.Web, :view
  use JaSerializer.PhoenixView

  attributes [:name]
end