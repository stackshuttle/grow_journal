defmodule GrowJournal.API.PestView do
  use GrowJournal.Web, :view
  use JaSerializer.PhoenixView

  attributes [:name]
end
