defmodule GrowJournal.API.PlantView do
  use GrowJournal.Web, :view
  use JaSerializer.PhoenixView

  attributes [:name, :description, :picture]
  has_many :diseases,
    serializer: GrowJournal.API.DiseaseSerializer,
    include: true
  has_many :pests,
    serializer: GrowJournal.API.PestSerializer,
    include: true
  has_many :varieties,
    serializer: GrowJournal.API.VarietySerializer,
    include: true
end