defmodule GrowJournal.API.VarietyView do
  use GrowJournal.Web, :view
  use JaSerializer.PhoenixView

  attributes [:name]
end
