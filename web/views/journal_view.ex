defmodule GrowJournal.JournalView do
  use GrowJournal.Web, :view

  def number_entries(journal) do
    Enum.count journal.entries
  end

  def last_picture([]) do
    nil
  end

  def last_picture([head|tail]) do
    cond do
      head.picture_path != nil ->
        head
      true ->
        last_picture(tail)
    end
  end
end
